USE [BstowDatabase]
GO

/****** Object: Table [dbo].[Tasks] Script Date: 8/13/2016 4:53:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[Tasks];


GO
CREATE TABLE [dbo].[Tasks] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Title]       NVARCHAR (MAX) NULL,
    [Description] NVARCHAR (MAX) NULL,
    [DueDate]     DATETIME       NULL,
    [Status]      NVARCHAR (MAX) NULL
);


