USE [BstowDatabase]
GO

/****** Object: Table [dbo].[Users] Script Date: 8/13/2016 4:53:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[Users];


GO
CREATE TABLE [dbo].[Users] (
    [Id]    INT           IDENTITY (1, 1) NOT NULL,
    [Email] NVARCHAR (50) NOT NULL
);


