namespace ToDo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Note : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notes", "Status", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notes", "Status");
        }
    }
}
