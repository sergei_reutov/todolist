﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ToDo.Models
{
    public class TaskViewModel
    {
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Description (optional)")]
        public string Description { get; set; }

        [Display(Name = "DueDate (optional)")]
        public DateTime? DueDate { get; set; }

    }
}