﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ToDo.Models;

namespace Bstow.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {

        public AccountController()
        {
        }


        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {        
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ViewBag.ReturnUrl = "/";
                UserContext db = new UserContext();
                var UserName = db.Users.Where(i => i.Email == model.Email).FirstOrDefault();
                if (UserName != null)
                {
                    FormsAuthentication.SetAuthCookie(model.Email, false);
                    
                    return Redirect("/");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return View();
            }
        }
        
        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User { Email = model.Email };
                UserContext db = new UserContext();
                try
                {
                    db.Users.Add(user);
                    db.SaveChanges();

                    return Redirect("/Account/Login");
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }

}