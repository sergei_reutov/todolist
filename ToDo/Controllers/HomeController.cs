﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDo.Models;

namespace ToDo.Controllers
{

     
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            UserContext db = new UserContext();

            return View(db.Users.ToList());
        }
    }
}