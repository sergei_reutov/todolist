﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDo.Models;

namespace ToDo.Controllers
{

     
    public class TaskController : Controller
    {
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                TaskContext db = new TaskContext();
                return View(db.Tasks.ToList());
            }
            else
            {
                return RedirectToAction("/Account/Login");
            }
        }

        public ActionResult Create()
        {
            if (Request.IsAuthenticated)
            {
                return View("Create");
            }
            else
            {
                return RedirectToAction("/Account/Login");
            }
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TaskViewModel model)
        {
            if (Request.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    var task = new Task { Title = model.Title, Description = model.Description, DueDate = model.DueDate, Status = "In progress" };
                    TaskContext db = new TaskContext();
                    try
                    {
                        db.Tasks.Add(task);
                        db.SaveChanges();

                        return Redirect("/Task/Index");
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
            

            // If we got this far, something failed, redirect to login
            return RedirectToAction("/Account/Login");
        }

    }
}